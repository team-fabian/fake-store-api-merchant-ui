import "./App.css";
import Allproducts from "./components/Allproducts/Allproducts";
import React, { Component } from "react";
import { Route, Routes } from "react-router-dom";
import ProductForm from "./components/Productform/Productform";
import Navbar from "./components/Navbar/Navbar";
import EditProductForm from "./components/EditProductForm/EditProductForm";
class App extends Component {
  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",

      LOADED: "loaded",

      ERROR: "error",
    };

    this.state = {
      totalProducts: [],

      status: this.API_STATES.LOADING,

      errorMessage: "",
    };

    this.url = "https://fakestoreapi.com/products";
  }

  getdata = (userdata) => {
    const dataarray = { ...userdata, id: this.state.totalProducts.length + 1 };
    console.log(dataarray);
    this.setState(
      {
        totalProducts: [...this.state.totalProducts, dataarray],
      },
      () => {
        console.log(this.state.totalProducts);
      }
    );
  };
  fetchData = (url) => {
    this.setState(
      {
        status: this.API_STATES.LOADING,
      },
      () => {
        fetch(url)
          .then((response) => response.json())
          .then((data) => {
            this.setState({
              status: this.API_STATES.LOADED,
              totalProducts: data,
            });
          })
          .catch((error) => {
            this.setState({
              status: this.API_STATES.ERROR,
              errorMessage:
                "An API error occurred. Please try again in a few minutes.",
            });
          });
      }
    );
  };
  componentDidMount = () => {
    this.fetchData(this.url);
  };

  handleDelete = (id) => {
    this.setState({
      totalProducts: [
        ...this.state.totalProducts.filter((item) => {
          return item.id !== id;
        }),
      ],
    });
  };

  handleProductUpdate = (updatedProduct) => {
    const updatedProducts = this.state.totalProducts.map((product) =>
      product.id === updatedProduct.id ? updatedProduct : product
    );
    this.setState({
      ...this.state.totalProducts,
      totalProducts: updatedProducts,
    });
  };

  render() {
    return (
      <div className="app">
        <Navbar />
        <Routes>
          <Route
            path="/"
            element={
              <Allproducts
                data={this.state.totalProducts}
                handleDelete={this.handleDelete}
              />
            }
          />
          <Route
            exact
            path="/addProduct"
            element={<ProductForm getdata={this.getdata} />}
          />

          <Route
            path="/updateProduct/:productId"
            element={
              <EditProductForm
                productsDetails={this.state}
                handleProductUpdate={this.handleProductUpdate}
              />
            }
          />
        </Routes>
      </div>
    );
  }
}

export default App;
