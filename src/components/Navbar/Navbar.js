import React, { Component } from "react";
import "./Navbar.css";

class Navbar extends Component {
  render() {
    return (
      <div className="navbar">
        <i className="fa-solid fa-shirt"></i>
        <h1>Myntra</h1>
      </div>
    );
  }
}
export default Navbar;
