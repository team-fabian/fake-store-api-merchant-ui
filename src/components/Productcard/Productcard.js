import React, { Component } from "react";
import "./Productcard.css";
import { Link } from "react-router-dom";

export default class Productcard extends Component {
  render() {
    let { handleDelete } = this.props;

    const { id, category, description, image, price, title, rating } =
      this.props.data;
    return (
      <div className="productcard">
        <img className="productImage" src={image} alt={title}></img>
        <span className="title">{title}</span>
        <div className="productDescription">{description.slice(0, 100)}</div>
        <h3 className="category">{category}</h3>
        <div className="customerRatings">
          <h4 className="rating">
            <i className="fa-solid fa-star"></i>
            {rating.rate}
          </h4>
          <h4 style={{ color: "blue" }}>({rating.count})</h4>
        </div>
        <span className="price">${price}</span>

        <div>
        <button className="deletebtn" onClick={() => handleDelete(id)}>delete</button>
        <Link to={`/updateProduct/${id}`}>
          <button className="updatebtn">Edit</button>
        </Link>
        </div>
      </div>
    );
  }
}
