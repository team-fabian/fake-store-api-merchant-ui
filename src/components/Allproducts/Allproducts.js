import React, { Component } from "react";
import Productcard from "../Productcard/Productcard";
import { Link } from "react-router-dom";
import './Allproducts.css'
class Allproducts extends Component {
  render() {
    const data = this.props.data;

    const allProducts = data.map((product) => {
      return (
        <Productcard
          key={product.id}
          data={product}
          handleDelete={this.props.handleDelete}
        />
      );
    });

    return (
      <>
      <div className="AddProduct">
      <Link to={"/addProduct"}>
          <button className="addproduct">Add Product</button>
        </Link>
      </div>
    
      <div className="allProducts" >
       

        {allProducts}
      </div>
      </>
    );
  }
}
export default Allproducts;
