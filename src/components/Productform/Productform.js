import React, { Component } from "react";
import "./Productform.css";
import validator from "validator";

export default class Productform extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: "",
      category: "",
      image: "",
      description: "",
      price: "",
      errors: {},
      rating: "",
    };
  }

  handleInputChange = (event) => {
    const { name, value } = event.target;

    this.setState({ [name]: value });
  };
  handleSubmit = (event) => {
    event.preventDefault();

    const { title, category, description, image, price } = this.state;
    let errors = {};

    if (validator.isEmpty(title)) {
      errors.title = "First Name Field can't be blank";
    } else if (!validator.isAlpha(title)) {
      errors.title = "First Name must contain only alphabets";
    }

    if (!validator.isURL(image)) {
      errors.image = "please enter valid URL";
    }

    if (!validator.isAlpha(description)) {
      errors.description = "please enter valid description";
    }

    if (!validator.isAlpha(category)) {
      errors.category = "please enter valid category";
    }

    if (!validator.isNumeric(price)) {
      errors.price = "please enter valid price";
    }

    if (Object.keys(errors).length > 0) {
      this.setState({ errors });
      return;
    }
    console.log("submitted");
    this.props.getdata(this.state);
  };

  render() {
    const { category, description, image, price, title, rating, errors } =
      this.state;
    return (
      <div>
        {true ? (
          <form className="addProduct" onSubmit={this.handleSubmit}>
            <div className="inputfield">
              <label className="property">Insert Title</label>
              <input
                className="inputbar"
                type="text"
                name="title"
                placeholder="Insert Product Title"
                value={title}
                onChange={this.handleInputChange}
              ></input>
            </div>
            {errors.title && <p>{errors.title}</p>}

            <div className="inputfield">
              <label className="property">Insert Image link</label>
              <input
                className="inputbar"
                type="text"
                name="image"
                placeholder="Insert Product Title"
                value={image}
                onChange={this.handleInputChange}
              ></input>
            </div>
            {errors.image && <p>{errors.image}</p>}

            <div className="inputfield">
              <label className="property">Insert Description</label>
              <input
                className="inputbar"
                type="text"
                name="description"
                placeholder="Insert Description."
                value={description}
                onChange={this.handleInputChange}
              ></input>
            </div>
            {errors.description && <p>{errors.description}</p>}

            <div className="inputfield">
              <label className="property">Insert Category</label>
              <input
                className="inputbar"
                type="text"
                name="category"
                placeholder="Insert Category"
                value={category}
                onChange={this.handleInputChange}
              ></input>
            </div>
            {errors.category && <p>{errors.category}</p>}

            <div className="inputfield">
              <label className="property">Insert price</label>
              <input
                className="inputbar"
                type="text"
                name="price"
                placeholder="Insert price"
                value={price}
                onChange={this.handleInputChange}
              ></input>
              {errors.price && <p>{errors.price}</p>}
            </div>

            <button className="submitbutton" type="submit">
              Submit
            </button>
          </form>
        ) : (
          <h1>Loading!!!!!!!</h1>
        )}
      </div>
    );
  }
}
