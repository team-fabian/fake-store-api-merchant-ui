import React, { Component } from "react";
import "./EditProductForm.css";

class EditProductForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: null,
      editFlag: false,
    };
  }

  getProduct = (id) => {
    console.log(id);
    const productData = this.props.productsDetails.totalProducts.find(
      (currentProduct) => {
        return currentProduct.id.toString() === id.toString();
      }
    );

    this.setState({
      ...this.state.product,
      product: productData,
    });
  };

  componentDidMount() {
    const id = window.location.href.split("/").slice(4);
    this.getProduct(id);
    console.log(id);
  }

  handleUpdateChange = (event) => {
    let name = event.target.name;
    let value = event.target.value;
    this.setState((prevState) => ({
      product: { ...prevState.product, [name]: value },
    }));
  };

  editHandleSubmit = (event) => {
    event.preventDefault();
    this.props.handleProductUpdate(this.state.product);
    this.setState((prevState) => ({
      editFlag: { ...prevState, dataFlag: true },
    }));
  };

  render() {
    return this.state.product === null || this.state.product === undefined ? (
      <h1 className="edit-message">Go to home page</h1>
    ) : (
      <div className="editItem">
        <form onSubmit={this.editHandleSubmit}>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <input
              type="text"
              className="form-control mt-1"
              name="title"
              placeholder="Enter title"
              value={this.state.product.title}
              onChange={(event) => this.handleUpdateChange(event)}
              required
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="category">Category</label>
            <input
              type="text"
              className="form-control mt-1"
              name="category"
              placeholder="Enter category"
              value={this.state.product.category}
              required
              onChange={this.handleUpdateChange}
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="price">Price</label>
            <input
              type="number"
              className="form-control mt-1"
              name="price"
              placeholder="Enter price"
              value={this.state.product.price}
              required
              onChange={this.handleUpdateChange}
            />
          </div>
          <div className="form-group mt-3">
            <label htmlFor="imageurl">Image URL</label>
            <input
              type="text"
              className="form-control mt-1"
              name="image"
              placeholder="Enter image URL"
              value={this.state.product.image}
              required
              onChange={this.handleUpdateChange}
            />
          </div>
          <button type="submit" className="btn btn-primary mt-4">
            Add Item
          </button>
          {this.state.editFlag ? <p>Data updated succesfully</p> : ""}
        </form>
      </div>
    );
  }
}

export default EditProductForm;
